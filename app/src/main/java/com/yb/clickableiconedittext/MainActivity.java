package com.yb.clickableiconedittext;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ClickableIconEditText editText = findViewById(R.id.editText);

        editText.setIconClickListener(() -> {
            Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
        });
    }
}
