package com.yb.clickableiconedittext;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.MotionEvent;

@SuppressLint("ClickableViewAccessibility")
public class ClickableIconEditText extends AppCompatEditText {
    private OnInfoClickListener listener;

    public ClickableIconEditText(Context context) {
        super(context);
    }

    public ClickableIconEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ClickableIconEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setIconClickListener(@NonNull OnInfoClickListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Drawable drawable = getCompoundDrawables()[2];
        if (drawable != null) {
            int drawableRightX = getWidth() - getPaddingRight();
            int drawableLeftX = drawableRightX - drawable.getBounds().width();
            boolean clicked = event.getX() >= drawableLeftX && event.getX() <= drawableRightX;

            if (event.getAction() == MotionEvent.ACTION_DOWN && clicked) {
                if (listener != null) {
                    listener.onInfoClick();
                }
                return true;
            }
        }
        return super.onTouchEvent(event);
    }

    public interface OnInfoClickListener {
        void onInfoClick();
    }
}

